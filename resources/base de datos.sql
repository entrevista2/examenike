create DATABASE escuela;

SHOW DATABASES;

USE escuela;

CREATE TABLE materia (
id INT auto_increment PRIMARY KEY,
nombre VARCHAR(50) NOT NULL
);

CREATE TABLE alumnos (
id INT auto_increment PRIMARY KEY,
nombre VARCHAR(50) NOT NULL,
apellidoPaterno varchar(50) NOT NULL,
apellidoMaterno varchar(50) NOT NULL
);

CREATE TABLE calificaciones (
id_calificacion INT AUTO_INCREMENT PRIMARY KEY,
id_alumno INT,
ID_MATERIA INT,
calificacion DECIMAL (4,2) NOT NULL,
FOREIGN KEY (id_alumno) references alumnos(id),
foreign key (id_materia) references materia(id)
);

GRANT ALL PRIVILEGES ON escuela. * to 'administrador1'@'localhost';

INSERT para la tabla materia
INSERT INTO materia (nombre) VALUES
('español'),
('matemáticas'),
('química');

INSERT para la tabla alumnos
INSERT INTO alumnos (nombre, apellidoPaterno, apellidoMaterno) VALUES
('Juan', 'Gómez', 'López'),
('María', 'Martínez', 'García'),
('Pedro', 'Sánchez', 'Rodríguez');

INSERT para la tabla calificaciones
INSERT INTO calificaciones (id_alumno, id_materia, calificacion) VALUES
(1, 1, 8.5),
(2, 2, 9.0),
(3, 3, 7.5);
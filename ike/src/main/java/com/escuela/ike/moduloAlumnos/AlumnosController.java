package com.escuela.ike.moduloAlumnos;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/escuela/alumnos")
public class AlumnosController {

    private final AlumnosService alumnosService;


    @Autowired
    public AlumnosController(AlumnosService alumnosService) {
        this.alumnosService = alumnosService;
    }

    @PostMapping("/alta")
    public Boolean createAlumno (@RequestBody AlumnosDTO alumnosDTO) {
        AlumnosEntity alumnosEntity = alumnosService.convertirDTOaEntity(alumnosDTO);
        System.out.println("Mi Entiti: " + alumnosEntity);
        return alumnosService.crearAlumno(alumnosEntity);
    }

    @GetMapping("/consulta")
    public ResponseEntity<AlumnosEntity> consultaAlumno (@PathVariable String matricula) {
        AlumnosEntity alumno = alumnosService.consultaMatricula(matricula);

        if (alumno == null) {
            return ResponseEntity.notFound().build();
        }
        return ResponseEntity.ok(alumno);
    }
}

package com.escuela.ike.moduloAlumnos;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Random;

@Service
public class AlumnosService {

    private final AlumnosRepository alumnosRepository;

    @Autowired
    public AlumnosService(AlumnosRepository alumnosRepository) {
        this.alumnosRepository = alumnosRepository;
    }

    public Boolean crearAlumno (AlumnosEntity alumnosEntity) {
        List<AlumnosEntity> alumnos = alumnosRepository.findByNombre(alumnosEntity.getNombre());
        System.out.println("Alumnos: " + alumnos);
        if (alumnos.isEmpty()) {
            alumnosRepository.save(alumnosEntity);
            return true;
        }else {
            return false;
        }
    }

    public AlumnosEntity convertirDTOaEntity(AlumnosDTO alumnosDTO) {

        AlumnosEntity alumnosEntity = new AlumnosEntity();
        alumnosEntity.setNombre(alumnosDTO.getNombre());
        alumnosEntity.setApellidoPaterno(alumnosDTO.getApellidoPaterno());
        alumnosEntity.setApellidoMaterno(alumnosDTO.getApellidoMaterno());
        alumnosEntity.setIdMateria(alumnosDTO.getIdMateria());
        alumnosEntity.setMatricula(creaMatricula(alumnosDTO.getNombre(), alumnosDTO.getApellidoPaterno(), alumnosDTO.getApellidoMaterno()));

        return alumnosEntity;
    }

    public String creaMatricula (String nombre, String apellidoPaterno, String apellidoMaterno) {
        String matricula = nombre.substring(0,2) + apellidoPaterno.substring(0,2) + apellidoMaterno.substring(0,2);
        Random random = new Random();
        String numeroAleatorio = String.valueOf(random.nextInt(100));
        System.out.println("matricula: " + matricula + numeroAleatorio);
        return matricula + numeroAleatorio;
    }

    public AlumnosEntity consultaMatricula (String matricula) {
        AlumnosEntity alumno = alumnosRepository.findByMatricula(matricula);
        return alumno;
    }
}

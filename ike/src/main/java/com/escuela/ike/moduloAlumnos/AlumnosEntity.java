package com.escuela.ike.moduloAlumnos;
import com.escuela.ike.calificaciones.CalificacionesEntity;
import jakarta.persistence.*;
import lombok.*;

import java.util.List;

@Entity
@Table(name="alumnos")
@NoArgsConstructor
@AllArgsConstructor
@ToString
@Getter
@Setter
public class AlumnosEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @Column (name = "nombre")
    private String nombre;

    @Column (name = "apellidoPaterno")
    private String apellidoPaterno;

    @Column (name = "apellidoMaterno")
    private String apellidoMaterno;

    @Column(name = "id_materia")
    private String idMateria;

    @Column(name = "matricula")
    private String matricula;

    @OneToMany(mappedBy = "alumnosEntity")
    private List<CalificacionesEntity> caalificaciones;
}

package com.escuela.ike.moduloAlumnos;

import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface AlumnosRepository extends JpaRepository<AlumnosEntity, Integer> {

    List<AlumnosEntity> findByNombre (String nombre);

    AlumnosEntity findByMatricula (String matricula);

    List<AlumnosEntity> findAll ();


}

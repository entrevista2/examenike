package com.escuela.ike.calificaciones;

import com.escuela.ike.moduloAlumnos.AlumnosEntity;
import com.escuela.ike.moduloAlumnos.AlumnosRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CalificacionesServicio {

    private final CalificacionesRepository calificacionesRepository;


    @Autowired
    public CalificacionesServicio(CalificacionesRepository calificacionesRepository) {
        this.calificacionesRepository = calificacionesRepository;

    }

    public List<CalificacionesEntity> calificacionAlumno () {
        return calificacionesRepository.findAll();
    }
}

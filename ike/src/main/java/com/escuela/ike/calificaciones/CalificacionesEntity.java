package com.escuela.ike.calificaciones;

import com.escuela.ike.materia.MateriaEntity;
import com.escuela.ike.moduloAlumnos.AlumnosEntity;
import com.fasterxml.jackson.annotation.JsonIgnore;
import jakarta.persistence.*;
import lombok.*;

@Entity
@Table(name="calificaciones")
@NoArgsConstructor
@AllArgsConstructor
@ToString
@Getter
@Setter
public class CalificacionesEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_calificacion")
    public int idCalificacion;




    @ManyToOne
    @JoinColumn(name = "id_alumno")
    @JsonIgnore
    private AlumnosEntity alumnosEntity;

    @ManyToOne
    @JoinColumn(name = "ID_MATERIA")
    private MateriaEntity materiaEntity;
}

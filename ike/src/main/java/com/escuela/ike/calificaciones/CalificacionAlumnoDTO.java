package com.escuela.ike.calificaciones;

public class CalificacionAlumnoDTO {

    private String nombreAlumno;

    private String nombreMateria;

    private double calificacion;

    public CalificacionAlumnoDTO(String nombreAlumno, String nombreMateria, double calificacion) {
        this.nombreAlumno = nombreAlumno;
        this.nombreMateria = nombreMateria;
        this.calificacion = calificacion;
    }

    public String getNombreAlumno() {
        return nombreAlumno;
    }

    public void setNombreAlumno(String nombreAlumno) {
        this.nombreAlumno = nombreAlumno;
    }

    public String getNombreMateria() {
        return nombreMateria;
    }

    public void setNombreMateria(String nombreMateria) {
        this.nombreMateria = nombreMateria;
    }

    public double getCalificacion() {
        return calificacion;
    }

    public void setCalificacion(double calificacion) {
        this.calificacion = calificacion;
    }
}

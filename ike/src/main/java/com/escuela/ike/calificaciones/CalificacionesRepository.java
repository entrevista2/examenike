package com.escuela.ike.calificaciones;

import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface CalificacionesRepository extends JpaRepository<CalificacionesEntity, Integer> {

    List<CalificacionesEntity> findByIdCalificacion (int idCalificacion);

    List<CalificacionesEntity> findAll ();

}

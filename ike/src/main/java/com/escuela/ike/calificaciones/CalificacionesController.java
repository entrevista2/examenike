package com.escuela.ike.calificaciones;

import com.escuela.ike.moduloAlumnos.AlumnosEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/escuela/calificaciones")
public class CalificacionesController {

    private final CalificacionesServicio calificacionesServicio;


    @Autowired
    public CalificacionesController(CalificacionesServicio calificacionesServicio) {
        this.calificacionesServicio = calificacionesServicio;
    }

    @GetMapping("/consultar")
    public List<CalificacionesEntity> calificacionAlumnoAll () {
        return calificacionesServicio.calificacionAlumno();
    }
}

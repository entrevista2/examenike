package com.escuela.ike.materia;

import com.escuela.ike.calificaciones.CalificacionesEntity;
import com.fasterxml.jackson.annotation.JsonIgnore;
import jakarta.persistence.*;
import lombok.*;

import java.util.List;

@Entity
@Table(name="materia")
@NoArgsConstructor
@AllArgsConstructor
@ToString
@Getter
@Setter
public class MateriaEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @Column(name = "nombre")
    private String nombre;

    @OneToMany(mappedBy = "materiaEntity")
    @JsonIgnore
    private List<CalificacionesEntity> calificaciones;
}
